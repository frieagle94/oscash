\babel@toc {italian}{}
\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Introduzione}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Assunzioni}{2}{subsection.1.1}%
\contentsline {section}{\numberline {2}Architettura dell'applicazione}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Architettura Distribuita}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Architettura Centralizzata}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Struttura della solution}{4}{subsection.2.3}%
\contentsline {section}{\numberline {3}Mockup e design dell'applicazione}{5}{section.3}%
\contentsline {section}{\numberline {4}Implementazione}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1}OSC01 - Aggiunta di prodotti alla lista dei prodotti selezionati}{8}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Implementazione tecnologica}{8}{subsubsection.4.1.1}%
\contentsline {subsection}{\numberline {4.2}OSC02 - Generazione scontrino}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}OSC04 - Autenticazione}{10}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Implementazione tecnologica}{10}{subsubsection.4.3.1}%
\contentsline {subsection}{\numberline {4.4}OSC05 - Rimozione di prodotti/menu dalla lista dei prodotti selezionati}{11}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Implementazione tecnologica}{11}{subsubsection.4.4.1}%
\contentsline {subsection}{\numberline {4.5}OSC06 - Aggiunta di menu alla lista dei prodotti selezionati}{12}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}Implementazione tecnologica}{12}{subsubsection.4.5.1}%
\contentsline {subsection}{\numberline {4.6}OSC11 - Genera ordine dallo scontrino}{13}{subsection.4.6}%
\contentsline {subsubsection}{\numberline {4.6.1}Implementazione tecnologica}{13}{subsubsection.4.6.1}%
\contentsline {subsection}{\numberline {4.7}OSC14 - Evidenza di disponibilità/indisponibilità di un prodotto/menu per la selezione}{14}{subsection.4.7}%
\contentsline {subsubsection}{\numberline {4.7.1}Implementazione tecnologica}{14}{subsubsection.4.7.1}%
\contentsline {section}{\numberline {5}Conclusioni e possibili sviluppi}{15}{section.5}%
\contentsline {subsection}{\numberline {5.1}Possibili Sviluppi}{15}{subsection.5.1}%
