using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using webapi.Models;

namespace webapi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            // Want to seed data on DB?
            bool seedData = true;

            CreateDbIfNotExists(host, seedData);

            host.Run();
        }
        /// <summary>
        /// Check if DB exists and seed data if empty
        /// </summary>
        /// <param name="host"></param>
        /// <param name="seedData">Check if must seed data</param>
        private static void CreateDbIfNotExists(IHost host, bool seedData)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<OSCashDBContext>();
                    using (context)
                    {
                        context.Database.EnsureCreated();

                        // Invalid all existing valid sessions at startup
                        var sessions = context.Sessions.Where(session => session.IsValid);
                        if (sessions.Any())
                        {
                            foreach (Session session in sessions)
                            {
                                session.IsValid = false;
                                session.EndedAt = DateTime.Now;
                            }
                            context.Sessions.UpdateRange(sessions);
                            context.SaveChanges();
                        }

                        if (seedData)
                        {
                            if (!context.UserTypes.Any())
                            {
                                context.UserTypes.AddRange(
                                    new UserType()
                                    {
                                        UserTypeID = Guid.NewGuid(),
                                        IsAdmin = true,
                                        Name = "Administrator"
                                    },
                                    new UserType()
                                    {
                                        UserTypeID = Guid.NewGuid(),
                                        IsAdmin = false,
                                        Name = "Operator"
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.Users.Any())
                            {
                                PasswordHasher hasher = new PasswordHasher(new HashingOptions());

                                context.Users.AddRange(
                                    new User()
                                    {
                                        UserID = Guid.NewGuid(),
                                        Name = "admin",
                                        Login = "admin",
                                        Password = hasher.Hash("admin"),
                                        UserTypeID = context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID
                                    },

                                    new User()
                                    {
                                        UserID = Guid.NewGuid(),
                                        Name = "user",
                                        Login = "user",
                                        Password = hasher.Hash("user"),
                                        UserTypeID = context.UserTypes.FirstOrDefault(type => !type.IsAdmin).UserTypeID
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.ProductTypes.Any())
                            {
                                context.ProductTypes.AddRange(
                                    new ProductType()
                                    {
                                        ProductTypeID = Guid.NewGuid(),
                                        Name = "Bevanda",
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID
                                    },

                                    new ProductType()
                                    {
                                        ProductTypeID = Guid.NewGuid(),
                                        Name = "Cibo",
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.Products.Any())
                            {
                                context.Products.AddRange(
                                    new Product()
                                    {
                                        ProductID = Guid.NewGuid(),
                                        Name = "Hamburger",
                                        ProductTypeID = context.ProductTypes.FirstOrDefault(type => type.Name == "Cibo").ProductTypeID,
                                        Description = "Panino con hamburger",
                                        Avalaible = true,
                                        Price = 4.00,
                                        StockAvalaible = 10,
                                        CanCustomize = false,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID
                                    },

                                    new Product()
                                    {
                                        ProductID = Guid.NewGuid(),
                                        Name = "Hot Dog",
                                        ProductTypeID = context.ProductTypes.FirstOrDefault(type => type.Name == "Cibo").ProductTypeID,
                                        Description = "Panino con wurstel",
                                        Avalaible = false,
                                        Price = 4.00,
                                        StockAvalaible = 0,
                                        CanCustomize = false,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                    },

                                    new Product()
                                    {
                                        ProductID = Guid.NewGuid(),
                                        Name = "Birra piccola",
                                        ProductTypeID = context.ProductTypes.FirstOrDefault(type => type.Name == "Bevanda").ProductTypeID,
                                        Description = "Birra da 0,2cl",
                                        Avalaible = true,
                                        Price = 3.00,
                                        StockAvalaible = 100,
                                        CanCustomize = false,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.Menus.Any())
                            {
                                context.Menus.AddRange(
                                    new Menu()
                                    {
                                        MenuID = Guid.NewGuid(),
                                        Name = "Hamburger e birra",
                                        Description = "Panino con hamburger e birra da 0,2cl",
                                        Avalaible = true,
                                        Price = 6.00,
                                        Customizable = true,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        ProductsAssociated = new List<Menu_Product>() {
                                            new Menu_Product() {
                                                ProductID = context.Products.FirstOrDefault(prod => prod.Name == "Hamburger").ProductID
                                            },
                                            new Menu_Product() {
                                                ProductID = context.Products.FirstOrDefault(prod => prod.Name == "Birra piccola").ProductID
                                            },
                                        }
                                    },

                                    new Menu()
                                    {
                                        MenuID = Guid.NewGuid(),
                                        Name = "Hot dog e birra",
                                        Description = "Panino con wurstel e birra da 0,2cl",
                                        Avalaible = false,
                                        Price = 6.00,
                                        Customizable = true,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        ProductsAssociated = new List<Menu_Product>() {
                                            new Menu_Product() {
                                                ProductID = context.Products.FirstOrDefault(prod => prod.Name == "Hot Dog").ProductID
                                            },
                                            new Menu_Product() {
                                                ProductID = context.Products.FirstOrDefault(prod => prod.Name == "Birra piccola").ProductID
                                            },
                                        }
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.Receipts.Any())
                            {
                                context.Receipts.AddRange(
                                    new Receipt()
                                    {
                                        ReceiptID = Guid.NewGuid(),
                                        Payed = true,
                                        Discount = 0,
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        MenusAssociated = new List<Menu_Receipt>() {
                                            new Menu_Receipt() {
                                                MenuID = context.Menus.FirstOrDefault(menu => menu.Name == "Hot dog e birra").MenuID,
                                                Amount = 2,
                                                Notes = ""
                                            }
                                        },
                                        ProductsAssociated = new List<Product_Receipt>() {
                                            new Product_Receipt() {
                                                ProductID = context.Products.FirstOrDefault(prod => prod.Name == "Hot Dog").ProductID,
                                                Amount = 1,
                                                Notes = "Ben cotto!"
                                            }
                                        }
                                    }
                                );
                                context.SaveChanges();
                            }

                            if (!context.Orders.Any())
                            {
                                context.Orders.AddRange(
                                    new Order()
                                    {
                                        OrderID = Guid.NewGuid(),
                                        CreatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        UpdatedByID = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID).UserID,
                                        ReceiptID = context.Receipts.First().ReceiptID
                                    }
                                );
                                context.SaveChanges();
                            }
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred creating the DB.");
                    throw ex;
                }
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
