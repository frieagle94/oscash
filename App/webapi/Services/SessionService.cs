using System;
using System.Collections.Generic;
using System.Linq;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Sessions
    /// </summary>
    public class SessionService
    {
        /// <summary>
        /// App DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }
        /// <summary>
        /// Decoder fo password
        /// </summary>
        /// <returns></returns>
        private PasswordHasher decoder = new PasswordHasher(new HashingOptions());
        /// <summary>
        /// Public ctor
        /// </summary>
        public SessionService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// Create a valid session if user credentials are valid
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="password">Password</param>
        /// <returns></returns>
        public DataResponse<Session> Login(string login, string password)
        {
            using (context)
            {
                DataResponse<Session> response = new DataResponse<Session>();
                User userToLogin = context.Users.ToList().FirstOrDefault(user => decoder.Check(user.Password, password));

                if (userToLogin == null)
                {
                    response.data = null;
                    response.Message = "Login o password errati.";
                    response.Result = ResponseResult.Error;
                }
                else
                {
                    Session session = new Session()
                    {
                        SessionID = Guid.NewGuid(),
                        UserID = userToLogin.UserID,
                        IsValid = true,
                        StartedAt = DateTime.Now,
                        EndedAt = DateTime.Now.AddMinutes(30)
                    };
                    context.Sessions.Add(session);
                    context.SaveChanges();
                    context.Entry(session).Reference(session => session.User).Load();
                    context.Entry(session.User).Reference(user => user.UserType).Load();
                    
                    response.data = session;
                    response.Message = "";
                    response.Result = ResponseResult.Success;
                }

                return response;
            }
        }
        /// <summary>
        /// Checks if a user has a valid session
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataResponse<Session> IsUserLoggedIn(Guid userId)
        {
            using (context)
            {
                DataResponse<Session> response = new DataResponse<Session>();
                Session session = context.Sessions.FirstOrDefault(session => session.UserID == userId);
                
                if(session.IsValid) {
                    if(!(DateTime.Now <= session.EndedAt)) {
                        session.IsValid = false;
                        context.Sessions.Update(session);
                        context.SaveChanges();
                        session = null;
                    }
                } else {
                    session = null;
                }

                if (session != null)
                {
                    response.data = session;
                    response.Message = "";
                    response.Result = ResponseResult.Success;
                }
                else
                {
                    context.Entry(session).Reference(session => session.User).Load();
                    context.Entry(session.User).Reference(user => user.UserType).Load();
                    response.data = null;
                    response.Message = "L'utente non è loggato.";
                    response.Result = ResponseResult.Error;
                }

                return response;
            }
        }
        /// <summary>
        /// Checks if a session is valid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataResponse<Session> IsSessionValid(Guid sessionId)
        {
            using (context)
            {
                DataResponse<Session> response = new DataResponse<Session>();
                Session session = context.Sessions.FirstOrDefault(session => session.SessionID == sessionId);
                
                if(session.IsValid) {
                    if(!(DateTime.Now <= session.EndedAt)) {
                        session.IsValid = false;
                        context.Sessions.Update(session);
                        context.SaveChanges();
                        session = null;
                    }
                } else {
                    session = null;
                }
                
                if (session != null)
                {
                    context.Entry(session).Reference(session => session.User).Load();
                    context.Entry(session.User).Reference(user => user.UserType).Load();
                    response.data = session;
                    response.Message = "";
                    response.Result = ResponseResult.Success;
                }
                else
                {
                    response.data = null;
                    response.Message = "La sessione non è valida.";
                    response.Result = ResponseResult.Error;
                }

                return response;
            }
        }
        /// <summary>
        /// Invalidates a session and logs out user
        /// </summary>
        /// <param name="login"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public Response Logout(Guid userId, Guid sessionId)
        {
            using (context)
            {
                Response response = new Response();

                Session sessionToTerminate;

                if (sessionId != null && sessionId != Guid.Empty)
                {
                    sessionToTerminate = context.Sessions.FirstOrDefault(session => session.SessionID == sessionId && session.IsValid);
                }
                else
                {
                    sessionToTerminate = context.Sessions.FirstOrDefault(session => session.UserID == userId && session.IsValid);
                }

                if (sessionToTerminate != null)
                {
                    sessionToTerminate.IsValid = false;
                    sessionToTerminate.EndedAt = DateTime.Now;
                    context.Sessions.Update(sessionToTerminate);
                    context.SaveChanges(); response.Message = "Logout effettuato.";
                    response.Result = ResponseResult.Success;
                    return response;
                }
                else
                {
                    response.Message = "L'utente non ha effettuato il login.";
                    response.Result = ResponseResult.Warning;
                }

                return response;
            }
        }
    }
}