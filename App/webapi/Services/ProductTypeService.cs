using System;
using System.Collections.Generic;
using System.Linq;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Product types transactions
    /// </summary>
    public class ProductTypeService : IService<ProductType>
    {
        /// <summary>
        /// APP DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }
        /// <summary>
        /// Public ctor
        /// </summary>
        public ProductTypeService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// Returns all product types
        /// </summary>
        /// <returns></returns>
        public DataResponse<IEnumerable<ProductType>> GetAll()
        {
            using (context) {
                DataResponse<IEnumerable<ProductType>> response = new DataResponse<IEnumerable<ProductType>>();
                response.data = context.ProductTypes.ToList();
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Returns a product type by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<ProductType> GetById(Guid id)
        {
            using (context) {
                DataResponse<ProductType> response = new DataResponse<ProductType>();
                response.data = context.ProductTypes.FirstOrDefault(prod => prod.ProductTypeID == id);
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Create a product type
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public Response Create(ProductType prod)
        {
            using (context) {
                try
                {
                    context.ProductTypes.Add(prod);
                    context.SaveChanges();
                    return new Response()
                    {
                        Message = "ProductType added.",
                        Result = ResponseResult.Success
                    };
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Update a product type
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public Response Update(ProductType prod)
        {
            using (context) {
                try
                {
                    if (context.ProductTypes.FirstOrDefault(prod => prod.ProductTypeID == prod.ProductTypeID) != null)
                    {
                        context.ProductTypes.Update(prod);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "ProductType updated.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "ProductType not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Delete a product type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response Delete(Guid id)
        {
            using (context) {
                try
                {
                    ProductType prodToDelete = context.ProductTypes.FirstOrDefault(prod => prod.ProductTypeID == id);

                    if (prodToDelete != null)
                    {
                        context.ProductTypes.Remove(prodToDelete);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "ProductType delete.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "ProductType not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
    }
}