using System;
using System.Collections.Generic;
using System.Linq;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Menus
    /// </summary>
    public class MenuService : IService<Menu>
    {
        /// <summary>
        /// App DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }
        /// <summary>
        /// Public ctor
        /// </summary>
        public MenuService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// Returns all menus
        /// </summary>
        /// <returns></returns>
        public DataResponse<IEnumerable<Menu>> GetAll()
        {
            using (context) {
                DataResponse<IEnumerable<Menu>> response = new DataResponse<IEnumerable<Menu>>();
                var menus = context.Menus.ToList();
                foreach (var menu in menus)
                {
                    context.Entry(menu).Collection(menu => menu.ProductsAssociated).Load();
                }
                response.data = menus;
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Return a menu by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<Menu> GetById(Guid id)
        {
            using (context) {
                DataResponse<Menu> response = new DataResponse<Menu>();
                response.data = context.Menus.FirstOrDefault(menu => menu.MenuID == id);
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Create a menu
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public Response Create(Menu menu)
        {
            using (context) {
                try
                {
                    context.Menus.Add(menu);
                    context.SaveChanges();
                    return new Response()
                    {
                        Message = "Menu added.",
                        Result = ResponseResult.Success
                    };
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Update a menu
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public Response Update(Menu menu)
        {
            using (context) {
                try
                {
                    if (context.Menus.FirstOrDefault(menu => menu.MenuID == menu.MenuID) != null)
                    {
                        context.Menus.Update(menu);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Menu updated.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "Menu not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Delete a menu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response Delete(Guid id)
        {
            using (context) {
                try
                {
                    Menu menuToDelete = context.Menus.FirstOrDefault(menu => menu.MenuID == id);

                    if (menuToDelete != null)
                    {
                        context.Menus.Remove(menuToDelete);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Menu delete.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "Menu not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
    }
}