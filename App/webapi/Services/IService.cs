using System;
using System.Collections.Generic;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle T transactions
    /// </summary>
    /// <typeparam name="T">Type of entity</typeparam>
    public interface IService<T>
    {
        /// <summary>
        /// DB context
        /// </summary>
        /// <value></value>
        OSCashDBContext context { get; set; }
        /// <summary>
        /// Returns all instances of T
        /// </summary>
        /// <returns>List of entities</returns>
        DataResponse<IEnumerable<T>> GetAll();
        /// <summary>
        /// Return the instance of T with id
        /// </summary>
        /// <param name="id">id of entity</param>
        /// <returns></returns>
        DataResponse<T> GetById(Guid id);
        /// <summary>
        /// Create a new instance of T
        /// </summary>
        /// <param name="entity">Entity to save</param>
        /// <returns>Response of call</returns>
        Response Create(T entity);
        /// <summary>
        /// Update a instance of T
        /// </summary>
        /// <param name="entity">Entity to update</param>
        /// <returns>Response of call</returns>
        Response Update(T entity);
        /// <summary>
        /// Delete a instance of T
        /// </summary>
        /// <param name="id">ID of entity to delete</param>
        /// <returns>Response of call</returns>
        Response Delete(Guid id);
    }
}