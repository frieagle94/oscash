using System;
using System.Collections.Generic;
using System.Linq;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Products
    /// </summary>
    public class ProductService : IService<Product>
    {
        /// <summary>
        /// App DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }
        /// <summary>
        /// Public ctor
        /// </summary>
        public ProductService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// Returns all products
        /// </summary>
        /// <returns></returns>
        public DataResponse<IEnumerable<Product>> GetAll()
        {
            using (context) {
                DataResponse<IEnumerable<Product>> response = new DataResponse<IEnumerable<Product>>();
                response.data = context.Products.ToList();
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Return a product by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<Product> GetById(Guid id)
        {
            using (context) {
                DataResponse<Product> response = new DataResponse<Product>();
                response.data = context.Products.FirstOrDefault(prod => prod.ProductID == id);
                response.Message = "";
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Create a product
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public Response Create(Product prod)
        {
            using (context) {
                try
                {
                    context.Products.Add(prod);
                    context.SaveChanges();
                    return new Response()
                    {
                        Message = "Product added.",
                        Result = ResponseResult.Success
                    };
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="prod"></param>
        /// <returns></returns>
        public Response Update(Product prod)
        {
            using (context) {
                try
                {
                    if (context.Products.FirstOrDefault(prod => prod.ProductID == prod.ProductID) != null)
                    {
                        context.Products.Update(prod);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Product updated.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "Product not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response Delete(Guid id)
        {
            using (context) {
                try
                {
                    Product prodToDelete = context.Products.FirstOrDefault(prod => prod.ProductID == id);

                    if (prodToDelete != null)
                    {
                        context.Products.Remove(prodToDelete);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Product delete.",
                            Result = ResponseResult.Success
                        };
                    } else {
                        return new Response()
                        {
                            Message = "Product not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
    }
}