﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Order
    /// </summary>
    public class OrderService : IService<Order>
    {
        /// <summary>
        /// App DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }

        /// <summary>
        /// Public ctor
        /// </summary>
        public OrderService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// Create a order
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public Response Create(Order order)
        {
            using (context)
            {
                try
                {
                    // TODO fill new Order
                    ReceiptService receiptService = new ReceiptService();
                    if(receiptService.GetById(order.ReceiptID).Result == ResponseResult.Success)
                    {
                        order.OrderID = Guid.NewGuid();
                        order.CreatedAt = DateTime.Now;                      
                        // populate user things
                        User user = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID);
                        order.CreatedByID = user.UserID;
                        order.UpdatedByID = user.UserID;

                        // Set a variable to the Documents path.
                        string docPath = Environment.CurrentDirectory;                        
                        docPath += "\\OSCash_Orders";
                        Directory.CreateDirectory(docPath);                                                

                        string orderFile = "";
                        string firstRow = "Ordine: " + order.OrderID + " | " + order.CreatedAt + "\r\n";
                        orderFile += firstRow;

                        foreach(Product_Receipt pr in order.Receipt.ProductsAssociated)
                        {
                            orderFile += "Prod: "+pr.Product.Name + " | qtn. " + pr.Amount +"\r\n";                            
                        }

                        foreach(Menu_Receipt mp in order.Receipt.MenusAssociated)
                        {
                            orderFile += "Menu: "+mp.Menu.Name + " | qtn. "+ mp.Amount + "\r\n";
                        }
                        
                        orderFile += "Note: " + order.Receipt.Notes + "\r\n"+ new String('-',firstRow.Length);

                        using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "Order_"+ DateTime.Now.ToString("ddMMyy_hhmmss") + order.OrderID+".txt")))
                        {
                            outputFile.WriteLine(orderFile);
                        }

                        order.Receipt = null;

                        context.Orders.Add(order);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Order added.",
                            Result = ResponseResult.Success
                        };

                    }
                    else
                    {
                        throw new Exception("Not founded Receipt-ID");
                    }

                    
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Delete a order
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response Delete(Guid id)
        {
            using (context)
            {
                try
                {
                    Order orderToDelete = context.Orders.FirstOrDefault(order => order.OrderID == id);

                    if (orderToDelete != null)
                    {
                        context.Orders.Remove(orderToDelete);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Order delete.",
                            Result = ResponseResult.Success
                        };
                    }
                    else
                    {
                        return new Response()
                        {
                            Message = "Order not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Returns all orders
        /// </summary>
        /// <returns></returns>
        public DataResponse<IEnumerable<Order>> GetAll()
        {
            using (context)
            {
                DataResponse<IEnumerable<Order>> response = new DataResponse<IEnumerable<Order>>();
                response.data = context.Orders.ToList();
                response.Message = ""; // TODO implements message
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Returns all orders with Products and Menus
        /// </summary>
        /// <returns></returns>
        public DataResponse<string> GetQueue()
        {
            using (context)
            {                
                IEnumerable<Order> orders = context.Orders.ToList();
                string returnString = "";
                foreach(Order order in orders)
                {
                    order.Receipt = new ReceiptService().GetById(order.ReceiptID).data;
                    returnString += "Ordine n°: "+order.OrderID+"\r\nCreato: "+order.CreatedAt+"\r\n";

                    IEnumerable<Product> products = context.Products.Where(prod => prod.ReceiptsAssociated.Where(rec => rec.ReceiptID == prod.ProductID).Count() != 0);
                    IEnumerable<Menu> menus = context.Menus.Where(menu => menu.ProductsAssociated.Where(prodm => prodm.Product.ReceiptsAssociated.Where(prod => prod.ReceiptID == prodm.ProductID).Count() != 0).Count() != 0);

                    Receipt receipt = context.Receipts.Single(rec => rec.ReceiptID == order.ReceiptID);
                    

                    IEnumerable <Product_Receipt> product_Rece = context.Receipts.Single(rec => rec.ReceiptID == order.ReceiptID).ProductsAssociated.Where(pa => pa.ReceiptID == order.ReceiptID);
                    //IEnumerable<Menu_Receipt> menu_Receipts = context.Menus.Single(menu => menu.ReceiptsAssociated.Where().Count() != 0 )

                    if(product_Rece != null)
                    {
                        foreach(Product_Receipt pr in product_Rece)
                        {
                            returnString += pr.Product.Name + " - " + pr.Amount + "\r\n" + pr.Product.Description;
                        }
                    }

                    
                                        
                    returnString += "---------------------------\r\n";
                }

                DataResponse<string> response = new DataResponse<string>();
                response.data = returnString;
                response.Message = ""; // TODO implements message
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Return a order by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<Order> GetById(Guid id)
        {
            using (context)
            {
                DataResponse<Order> response = new DataResponse<Order>();
                response.data = context.Orders.FirstOrDefault(order => order.OrderID == id);
                response.Message = ""; // TODO implements message
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Update a order
        /// </summary>
        /// <param name="receipt"></param>
        /// <returns></returns>
        public Response Update(Order order)
        {
            using (context)
            {
                try
                {
                    if (context.Orders.FirstOrDefault(order => order.OrderID == order.OrderID) != null)
                    {
                        context.Orders.Update(order);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Order updated.",
                            Result = ResponseResult.Success
                        };
                    }
                    else
                    {
                        return new Response()
                        {
                            Message = "Order not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
    }
}
