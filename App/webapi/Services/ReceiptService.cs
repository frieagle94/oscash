﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Services
{
    /// <summary>
    /// Represent a service which handle Receipt
    /// </summary>
    public class ReceiptService : IService<Receipt>
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public ReceiptService()
        {
            context = new OSCashDBContext();
        }
        /// <summary>
        /// App DB Context
        /// </summary>
        /// <value></value>
        public OSCashDBContext context { get; set; }
        /// <summary>
        /// Create a receipt
        /// </summary>
        /// <param name="receipt"></param>
        /// <returns></returns>
        public Response Create(Receipt receipt)
        {
            using (context)
            {
                try
                {
                    receipt.ReceiptID = Guid.NewGuid();
                    receipt.CreatedAt = DateTime.Now;

                    if (receipt.Notes == null)
                    {
                        receipt.Notes = "No Note";
                    }
                    if (receipt.ProductsAssociated == null & receipt.MenusAssociated == null)
                    {
                        throw new Exception("No articles founded in lists");
                    }

                    // calculate total price of receipt and assign ProductID
                    double totalPrice = 0;
                    foreach (Product_Receipt product in receipt.ProductsAssociated)
                    {
                        totalPrice += product.Product.Price * product.Amount;
                        product.ProductID = product.Product.ProductID;
                        product.Product = null;
                        product.ReceiptID = receipt.ReceiptID;
                    }
                    foreach (Menu_Receipt menu in receipt.MenusAssociated)
                    {
                        totalPrice += menu.Menu.Price * menu.Amount;
                        menu.MenuID = menu.Menu.MenuID;
                        menu.Menu = null;
                        menu.ReceiptID = receipt.ReceiptID;
                    }

                    // Calculate percentual or flat discount applied
                    double totalPriceDiscountPercentual = totalPrice - ((totalPrice / 100) * receipt.Discount);
                    totalPriceDiscountPercentual = Math.Round(totalPriceDiscountPercentual, 2);
                    double totalPriceDiscountFlat = totalPrice - receipt.Discount;
                    totalPriceDiscountFlat = Math.Round(totalPriceDiscountFlat, 2);
                    if ((totalPriceDiscountFlat == receipt.TotalPrice) & (totalPriceDiscountFlat >= 0) & (totalPriceDiscountFlat <= totalPrice))
                    {
                        receipt.TotalPrice = totalPriceDiscountFlat;

                    }
                    else if ((totalPriceDiscountPercentual == receipt.TotalPrice) & (receipt.Discount <= 100) & (receipt.Discount >= 0))
                    {
                        receipt.TotalPrice = totalPriceDiscountPercentual;
                    }
                    else
                    {                        
                        throw new Exception("Invalid Discount mode or value");
                    }

                    // populate user things
                    User user = context.Users.FirstOrDefault(user => user.UserTypeID == context.UserTypes.FirstOrDefault(type => type.IsAdmin).UserTypeID);
                    receipt.CreatedByID = user.UserID;
                    receipt.UpdatedByID = user.UserID;

                    context.Receipts.Add(receipt);
                    context.SaveChanges();
                    return new Response()
                    {
                        Message = receipt.ReceiptID.ToString(),
                        Result = ResponseResult.Success
                    };
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Delete a receipt
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response Delete(Guid id)
        {
            using (context)
            {
                try
                {
                    Receipt receiptToDelete = context.Receipts.FirstOrDefault(receipt => receipt.ReceiptID == id);

                    if (receiptToDelete != null)
                    {
                        context.Receipts.Remove(receiptToDelete);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Receipt delete.",
                            Result = ResponseResult.Success
                        };
                    }
                    else
                    {
                        return new Response()
                        {
                            Message = "Receipt not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
        /// <summary>
        /// Returns all receipts
        /// </summary>
        /// <returns></returns>
        public DataResponse<IEnumerable<Receipt>> GetAll()
        {
            using (context)
            {
                DataResponse<IEnumerable<Receipt>> response = new DataResponse<IEnumerable<Receipt>>();
                response.data = context.Receipts.ToList();
                response.Message = ""; // TODO implements message
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Return a receipt by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public DataResponse<Receipt> GetById(Guid id)
        {
            using (context)
            {
                DataResponse<Receipt> response = new DataResponse<Receipt>();
                Receipt receipt = context.Receipts.FirstOrDefault(receipt => receipt.ReceiptID == id);
               
                response.data = receipt;
                response.Message = ""; // TODO implements message
                response.Result = ResponseResult.Success;
                return response;
            }
        }
        /// <summary>
        /// Update a receipt
        /// </summary>
        /// <param name="receipt"></param>
        /// <returns></returns>
        public Response Update(Receipt receipt)
        {
            using (context)
            {
                try
                {
                    if (context.Receipts.FirstOrDefault(receipt => receipt.ReceiptID == receipt.ReceiptID) != null)
                    {
                        context.Receipts.Update(receipt);
                        context.SaveChanges();
                        return new Response()
                        {
                            Message = "Receipt updated.",
                            Result = ResponseResult.Success
                        };
                    }
                    else
                    {
                        return new Response()
                        {
                            Message = "Receipt not existing.",
                            Result = ResponseResult.Error
                        };
                    }
                }
                catch (System.Exception ex)
                {
                    return new Response()
                    {
                        Message = ex.ToString(),
                        Result = ResponseResult.Error
                    };
                }
            }
        }
    }
}
