﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which moves Receipts
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiptController : ControllerBase, IControllerBase<Receipt>
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<ReceiptController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public ReceiptController(ILogger<ReceiptController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Delete a receipt
        /// </summary>
        /// <param name="id">Id of receipt to delete</param>
        /// <returns></returns>
        [HttpDelete]
        public Response Delete(Guid id)
        {
            ReceiptService receiptService = new ReceiptService();
            Response response = receiptService.Delete(id);
            return response;
        }
        /// <summary>
        /// Returns all receipts
        /// </summary>
        /// <returns>List of receipts</returns>
        [HttpGet]
        [Route("/Receipt/Get")]
        public DataResponse<IEnumerable<Receipt>> Get()
        {
            ReceiptService receiptService = new ReceiptService();
            DataResponse<IEnumerable<Receipt>> receipts = receiptService.GetAll();
            return receipts;
        }
        /// <summary>
        /// Return a receipt
        /// </summary>
        /// <param name="id">Id of receipt</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Receipt/GetSingle/{id}")]
        public DataResponse<Receipt> GetSingle(Guid id)
        {
            ReceiptService receiptService = new ReceiptService();
            DataResponse<Receipt> receipt = receiptService.GetById(id);
            return receipt;
        }
        /// <summary>
        /// Save a receipt
        /// </summary>
        /// <param name="receipt">Receipt to save</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/Receipt/New")]
        public Response Post(Receipt receipt)
        {
            ReceiptService receiptService = new ReceiptService();
            Response response = receiptService.Create(receipt);
            return response;
        }
        /// <summary>
        /// Update a receipt
        /// </summary>
        /// <param name="receipt">Receipt to update</param>
        /// <returns></returns>
        [HttpPut]
        public Response Put(Receipt receipt)
        {
            ReceiptService receiptService = new ReceiptService();
            Response response = receiptService.Update(receipt);
            return response;
        }
    }
}