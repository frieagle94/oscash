﻿using System;
using webapi.Models;
using webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which moves Menus
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class MenuController : ControllerBase, IControllerBase<Menu>
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<MenuController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public MenuController(ILogger<MenuController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Returns all menus
        /// </summary>
        /// <returns>List of menus</returns>
        [HttpGet]
        [Route("/Menu/Get")]
        public DataResponse<IEnumerable<Menu>> Get()
        {
            MenuService menuService = new MenuService();
            DataResponse<IEnumerable<Menu>> menus = menuService.GetAll();
            return menus;
        }
        /// <summary>
        /// Return a menu
        /// </summary>
        /// <param name="id">Id of menu</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Menu/GetSingle/{id}")]
        public DataResponse<Menu> GetSingle(Guid id)
        {
            MenuService menuService = new MenuService();
            DataResponse<Menu> menu = menuService.GetById(id);
            return menu;
        }
        /// <summary>
        /// Save a menu
        /// </summary>
        /// <param name="prod">Menu to save</param>
        /// <returns></returns>
        [HttpPost]
        public Response Post (Menu prod)
        {
            MenuService menuService = new MenuService();
            Response response = menuService.Create(prod);
            return response;
        }
        /// <summary>
        /// Update a menu
        /// </summary>
        /// <param name="prod">Menu to update</param>
        /// <returns></returns>
        [HttpPut]
        public Response Put (Menu prod)
        {
            MenuService menuService = new MenuService();
            Response response = menuService.Update(prod);
            return response;
        }
        /// <summary>
        /// Delete a menu
        /// </summary>
        /// <param name="id">Id of menu to delete</param>
        /// <returns></returns>
        [HttpDelete]
        public Response Delete (Guid id)
        {
            MenuService menuService = new MenuService();
            Response response = menuService.Delete(id);
            return response;
        }
    }
}
