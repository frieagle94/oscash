using System;
using webapi.Models;
using webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which moves ProductsTypes
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductTypeController : ControllerBase, IControllerBase<ProductType>
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<ProductTypeController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public ProductTypeController(ILogger<ProductTypeController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Returns all ProductTypes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/ProductType/Get")]
        public DataResponse<IEnumerable<ProductType>> Get()
        {
            ProductTypeService productTypeService = new ProductTypeService();
            DataResponse<IEnumerable<ProductType>> productTypes = productTypeService.GetAll();
            return productTypes;
        }
        /// <summary>
        /// Return a product type
        /// </summary>
        /// <param name="id">Id of product type</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/ProductType/GetSingle/{id}")]
        public DataResponse<ProductType> GetSingle(Guid id)
        {
            ProductTypeService productTypeService = new ProductTypeService();
            DataResponse<ProductType> ProductType = productTypeService.GetById(id);
            return ProductType;
        }
        /// <summary>
        /// Create a product type
        /// </summary>
        /// <param name="prod">Product type to create</param>
        /// <returns></returns>
        [HttpPost]
        public Response Post (ProductType prod)
        {
            ProductTypeService productTypeService = new ProductTypeService();
            Response response = productTypeService.Create(prod);
            return response;
        }
        /// <summary>
        /// Update a product type
        /// </summary>
        /// <param name="prod">Product type to update</param>
        /// <returns></returns>
        [HttpPut]
        public Response Put (ProductType prod)
        {
            ProductTypeService productTypeService = new ProductTypeService();
            Response response = productTypeService.Update(prod);
            return response;
        }
        /// <summary>
        /// Delete a product type
        /// </summary>
        /// <param name="id">Product type to delete</param>
        /// <returns></returns>
        [HttpDelete]
        public Response Delete (Guid id)
        {
            ProductTypeService productTypeService = new ProductTypeService();
            Response response = productTypeService.Delete(id);
            return response;
        }
    }
}
