using System;
using webapi.Models;
using webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which moves Sessions
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class SessionController : ControllerBase
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<SessionController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public SessionController(ILogger<SessionController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Login a user based on his credential
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/Session/Login")]
        public DataResponse<Session> Login(string login, string password)
        {
            SessionService sessionService = new SessionService();
            DataResponse<Session> session = sessionService.Login(login, password);
            return session;
        }
        /// <summary>
        /// Checks if a user is logged in
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Session/IsUserLoggedIn")]
        public DataResponse<Session> IsUserLoggedIn(Guid userId) {
            SessionService sessionService = new SessionService();
            DataResponse<Session> session = sessionService.IsUserLoggedIn(userId);
            return session;
        }
        /// <summary>
        /// Checks if a session is valid
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Session/IsSessionValid")]
        public DataResponse<Session> IsSessionValid(Guid sessionId) {
            SessionService sessionService = new SessionService();
            DataResponse<Session> session = sessionService.IsSessionValid(sessionId);
            return session;
        }
        /// <summary>
        /// Logout a user currently logged in
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Session/Logout")]
        public Response Logout(Guid userId, Guid sessionId)
        {
            SessionService sessionService = new SessionService();
            Response session = sessionService.Logout(userId, sessionId);
            return session;
        }
    }
}
