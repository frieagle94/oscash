using System;
using webapi.Models;
using webapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which moves Products
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase, IControllerBase<Product>
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<ProductController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public ProductController(ILogger<ProductController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Returns all products
        /// </summary>
        /// <returns>List of products</returns>
        [HttpGet]
        [Route("/Product/Get")]
        public DataResponse<IEnumerable<Product>> Get()
        {
            ProductService productService = new ProductService();
            DataResponse<IEnumerable<Product>> products = productService.GetAll();
            return products;
        }
        /// <summary>
        /// Return a product
        /// </summary>
        /// <param name="id">Id of product</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Product/GetSingle/{id}")]
        public DataResponse<Product> GetSingle(Guid id)
        {
            ProductService productService = new ProductService();
            DataResponse<Product> product = productService.GetById(id);
            return product;
        }
        /// <summary>
        /// Save a product
        /// </summary>
        /// <param name="prod">Product to save</param>
        /// <returns></returns>
        [HttpPost]
        public Response Post (Product prod)
        {
            ProductService productService = new ProductService();
            Response response = productService.Create(prod);
            return response;
        }
        /// <summary>
        /// Update a product
        /// </summary>
        /// <param name="prod">Product to update</param>
        /// <returns></returns>
        [HttpPut]
        public Response Put (Product prod)
        {
            ProductService productService = new ProductService();
            Response response = productService.Update(prod);
            return response;
        }
        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="id">Id of product to delete</param>
        /// <returns></returns>
        [HttpDelete]
        public Response Delete (Guid id)
        {
            ProductService productService = new ProductService();
            Response response = productService.Delete(id);
            return response;
        }
    }
}
