using System;
using webapi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a base controller
    /// </summary>
    /// <typeparam name="T">Type of entity moved by controller</typeparam>
    public interface IControllerBase<T>
    {
        /// <summary>
        /// Returns all entities T in db.
        /// </summary>
        /// <returns>List of entities</returns>
        [HttpGet]
        [Route("/{controller}/Get")]   
        DataResponse<IEnumerable<T>> Get();
        /// <summary>
        /// Returns all entities T in db with id
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>Entity</returns>
        [HttpGet]
        [Route("/{controller}/GetSingle")]
        DataResponse<T> GetSingle(Guid id);
        /// <summary>
        /// Save a new instance of T in db
        /// </summary>
        /// <param name="entity">Entity to save</param>
        /// <returns>Response of call</returns>
        [HttpPost]
        Response Post (T entity);
        /// <summary>
        /// Update a instance of T in db
        /// </summary>
        /// <param name="entity">Entity to update</param>
        /// <returns>Response of call</returns>
        [HttpPut]
        Response Put (T entity);
        /// <summary>
        /// Delete a instance of T in db
        /// </summary>
        /// <param name="id">Id of entity to delete</param>
        /// <returns>Response of call</returns>
        [HttpDelete]
        Response Delete (Guid id);
    }
}
