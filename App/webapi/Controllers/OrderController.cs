﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    /// <summary>
    /// Represent a controller which control an Order
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase, IControllerBase<Order>
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger<OrderController> _logger;
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <param name="logger"></param>
        public OrderController(ILogger<OrderController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// Delete an order
        /// </summary>
        /// <param name="id">Id of order to delete</param>
        /// <returns></returns>
        [HttpDelete]
        public Response Delete(Guid id)
        {
            OrderService orderService = new OrderService();
            Response response = orderService.Delete(id);
            return response;
        }
        /// <summary>
        /// Returns all orders
        /// </summary>
        /// <returns>List of orders</returns>
        [HttpGet]
        [Route("/Order/Get")]
        public DataResponse<IEnumerable<Order>> Get()
        {
            OrderService orderService = new OrderService();
            DataResponse<IEnumerable<Order>> orders = orderService.GetAll();
            return orders;
        }
        /// <summary>
        /// Return queued orders
        /// </summary>
        /// <returns>Queued orders</returns>
        [HttpGet]
        [Route("/Order/GetQueue")]
        public DataResponse<string> GetQueue()
        {
            OrderService orderService = new OrderService();
            DataResponse<string> orders = orderService.GetQueue();
            return orders;
        }
        /// <summary>
        /// Return an order
        /// </summary>
        /// <param name="id">Id of order</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/Order/GetSingle/{id}")]
        public DataResponse<Order> GetSingle(Guid id)
        {
            OrderService orderService = new OrderService();
            DataResponse<Order> order = orderService.GetById(id);
            return order;
        }
        /// <summary>
        /// Save an order
        /// </summary>
        /// <param name="prod">Order to save</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/Order/New")]
        public Response Post(Order order)
        {
            OrderService orderService = new OrderService();
            Response response = orderService.Create(order);
            return response;
        }
        /// <summary>
        /// Update an order
        /// </summary>
        /// <param name="order">Order to update</param>
        /// <returns></returns>
        [HttpPut]
        public Response Put(Order order)
        {
            OrderService orderService = new OrderService();
            Response response = orderService.Update(order);
            return response;
        }
    }
}