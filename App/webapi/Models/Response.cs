namespace webapi.Models
{
    /// <summary>
    /// Represent a service/controller response without data
    /// </summary>
    public class Response : IResponse
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public Response()
        {
        }
        /// <summary>
        /// Message of response
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
        /// <summary>
        /// Result of response
        /// </summary>
        /// <value></value>
        public ResponseResult Result { get; set; }
    }
}