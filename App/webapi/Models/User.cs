using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Represent a user of OSCash
    /// </summary>
    public class User
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public User()
        {
            CreatedAt = DateTime.Now;
        }
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserID { get; set; }
        /// <summary>
        /// Login
        /// </summary>
        /// <value></value>
        public string Login { get; set; }
        /// <summary>
        /// User type ID
        /// </summary>
        /// <value></value>
        public Guid UserTypeID { get; set; }
        /// <summary>
        /// Moment of creation
        /// </summary>
        /// <value></value>
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Password Hashcode
        /// </summary>
        /// <value></value>
        public string Password { get; set; }
        // Navigation properties
        /// <summary>
        /// Navigation to user type
        /// </summary>
        /// <value></value>
        public UserType UserType { get; set; }
    }
}