using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Represent a login session
    /// </summary>
    public class Session
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public Session()
        {
            StartedAt = DateTime.Now;
        }
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SessionID { get; set; }
        /// <summary>
        /// Id of user
        /// </summary>
        /// <value></value>
        public Guid UserID { get; set; }
        /// <summary>
        /// If is session valid
        /// </summary>
        /// <value></value>
        public bool IsValid { get; set; }
        /// <summary>
        /// Moment of start
        /// </summary>
        /// <value></value>
        public DateTime StartedAt {  get; set; }
        /// <summary>
        /// Moment of end
        /// </summary>
        /// <value></value>
        public DateTime? EndedAt { get; set; }

        // Navigation properties
        /// <summary>
        /// Navigation to user
        /// </summary>
        /// <value></value>
        public User User { get; set; }

    }
}