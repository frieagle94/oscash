using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    public class Menu_Receipt
    {
        [Key]
        public Guid MenuID { get; set; }
        [Key]
        public Guid ReceiptID { get; set; }
        public Menu Menu { get; set; }
        public Receipt Receipt { get; set; }
        public int Amount {get; set; }        
        public string Notes {get; set; }
    }
}