using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    public class Menu:BaseModel
    {
        public Menu() : base()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MenuID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public bool Avalaible { get; set; }
        public bool Customizable {  get; set; }
        public ICollection<Menu_Product> ProductsAssociated { get; set; }
        public ICollection<Menu_Receipt> ReceiptsAssociated { get; set; }
    }
}