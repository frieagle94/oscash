using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    public class Product:BaseModel
    {
        public Product() : base()
        {

        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ProductTypeID {  get; set; }
        public ProductType ProductType {  get; set; }
        public double Price { get; set; }
        public bool Avalaible { get; set; }
        public int StockAvalaible { get; set; }
        public bool CanCustomize { get; set; }
        public byte[] Image { get; set; }
        public ICollection<Menu_Product> MenusAssociated { get; set; }
        public ICollection<Product_Receipt> ReceiptsAssociated { get; set; }
    }
}