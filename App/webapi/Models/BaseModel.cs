using System;

namespace webapi.Models
{
    /// <summary>
    /// Represent a base model of app
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public BaseModel()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        /// <summary>
        /// Id of user who created entity
        /// </summary>
        /// <value></value>
        public Guid CreatedByID { get; set; }
        /// <summary>
        /// User who created entity
        /// </summary>
        /// <value></value>
        public User CreatedBy { get; set; }
        /// <summary>
        /// Id of User who updated entity
        /// </summary>
        /// <value></value>
        public Guid UpdatedByID { get; set; }
        /// <summary>
        /// User who updated entity
        /// </summary>
        /// <value></value>
        public User UpdatedBy { get; set; }
        /// <summary>
        /// Moment of creation
        /// </summary>
        /// <value></value>
        public DateTime CreatedAt {  get; set; }
        /// <summary>
        /// Moment of update
        /// </summary>
        /// <value></value>
        public DateTime UpdatedAt { get; set; }
    }
}