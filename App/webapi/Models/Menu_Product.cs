using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Relationship between menu and products (N-N)
    /// </summary>
    public class Menu_Product
    {
        /// <summary>
        /// Id of menu associated
        /// </summary>
        /// <value></value>
        [Key]
        public Guid MenuID { get; set; }
        /// <summary>
        /// Id of product associated
        /// </summary>
        /// <value></value>
        [Key]
        public Guid ProductID { get; set; }
        
        // Navigation properties
        /// <summary>
        /// Navigation to Menu
        /// </summary>
        /// <value></value>
        public Menu Menu { get; set; }
        /// <summary>
        /// Navigation to product
        /// </summary>
        /// <value></value>
        public Product Product { get; set; }
    }
}