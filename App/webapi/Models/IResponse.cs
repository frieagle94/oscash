namespace webapi.Models
{
    /// <summary>
    /// Enumeration of response result
    /// </summary>
    public enum ResponseResult
    {
        /// <summary>
        /// Ok
        /// </summary>
        Success = 0,
        /// <summary>
        /// Error
        /// </summary>
        Error = 1,
        /// <summary>
        /// Ok, but...
        /// </summary>
        Warning = 2
    }
    /// <summary>
    /// Represent a service/controller response
    /// </summary>
    public interface IResponse
    {
        /// <summary>
        /// Message of response
        /// </summary>
        /// <value></value>
        string Message { get; set; }
        /// <summary>
        /// Result of response
        /// </summary>
        /// <value></value>
        ResponseResult Result { get; set; }
    }
}