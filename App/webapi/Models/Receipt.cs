using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    public class Receipt:BaseModel
    {
        public Receipt() : base()
        {

        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ReceiptID { get; set; }
        public bool Payed { get; set; }
        public double TotalPrice { get; set; }
        public double Discount {  get; set; }
        public string Notes { get; set; }
        public ICollection<Product_Receipt> ProductsAssociated { get; set; }       
        public ICollection<Menu_Receipt> MenusAssociated { get; set; }       
    }
}