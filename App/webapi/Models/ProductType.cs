using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Represent a product type
    /// </summary>
    public class ProductType:BaseModel
    {
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ProductTypeID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
    }
}