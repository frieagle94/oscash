using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    public class Product_Receipt
    {
        [Key]
        public Guid ReceiptID { get; set; }
        [Key]
        public Guid ProductID { get; set; }
        public Receipt Receipt { get; set; }
        public Product Product { get; set; }
        public int Amount { get; set;}
        public string Notes {get; set; }
    }
}