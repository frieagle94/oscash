using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Represent a user type of OSCash
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public UserType()
        {
        }
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid UserTypeID { get; set;}
        /// <summary>
        /// If is type admin
        /// </summary>
        /// <value></value>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
    }
}