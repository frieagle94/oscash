using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapi.Models
{
    /// <summary>
    /// OSCash DB Context
    /// </summary>
    public class OSCashDBContext : DbContext
    {
        /// <summary>
        /// Set of User types
        /// </summary>
        /// <value></value>
        public DbSet<UserType> UserTypes { get; set; }
        /// <summary>
        /// Set of users
        /// </summary>
        /// <value></value>
        public DbSet<User> Users { get; set; }
        /// <summary>
        /// Set of sessions
        /// </summary>
        /// <value></value>
        public DbSet<Session> Sessions { get; set; }
        /// <summary>
        /// Set of orders
        /// </summary>
        /// <value></value>
        public DbSet<Order> Orders { get; set; }
        /// <summary>
        /// Set of receipts
        /// </summary>
        /// <value></value>
        public DbSet<Receipt> Receipts { get; set; }
        /// <summary>
        /// Set of menus
        /// </summary>
        /// <value></value>
        public DbSet<Menu> Menus { get; set; }
        /// <summary>
        /// Set of product types
        /// </summary>
        /// <value></value>
        public DbSet<ProductType> ProductTypes { get; set; }
        /// <summary>
        /// Set of products
        /// </summary>
        /// <value></value>
        public DbSet<Product> Products { get; set; }
        /// <summary>
        /// Public ctor
        /// </summary>
        public OSCashDBContext()
        {

        }
        /// <summary>
        /// Public ctor with options
        /// </summary>
        /// <param name="options">Options to ctor</param>
        /// <returns></returns>
        public OSCashDBContext(DbContextOptions<OSCashDBContext> options) : base(options)
        {
        }
        /// <summary>
        /// Create the DB schema
        /// </summary>
        /// <param name="modelBuilder">Obj used to create</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu_Product>()
                .HasKey(mp => new{ mp.MenuID, mp.ProductID})
                .HasName("PK_Menu_Prod");

            modelBuilder.Entity<Product_Receipt>()
                 .HasKey(pr => new{ pr.ProductID, pr.ReceiptID })
                 .HasName("PK_Prod_Receipt");
            
            modelBuilder.Entity<Menu_Receipt>()
                .HasKey(mr => new{ mr.MenuID, mr.ReceiptID })
                .HasName("PK_Menu_Receipt");
        }
        /// <summary>
        /// Configure option to use connection string
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=oscash-db;Username=admin-oscash;Password=oscashDB42");
    }
}