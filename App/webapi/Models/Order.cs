using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace webapi.Models
{
    /// <summary>
    /// Represent a order to kitchen
    /// </summary>
    public class Order:BaseModel
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        /// <returns></returns>
        public Order() : base()
        {

        }
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OrderID { get; set; }
        /// <summary>
        /// Id of receipt associated
        /// </summary>
        /// <value></value>
        public Guid ReceiptID { get; set; }
        /// <summary>
        /// User who completed order
        /// </summary>
        /// <value></value>
        public Guid? EndedByID { get; set; }
        /// <summary>
        /// Moment of complete
        /// </summary>
        /// <value></value>
        public DateTime EndedAt { get; set; }

        // Navigation properties
        /// <summary>
        /// Navigation to receipt
        /// </summary>
        /// <value></value>
        public Receipt Receipt { get; set; }
        /// <summary>
        /// Navigation to user
        /// </summary>
        /// <value></value>
        public User EndedBy { get; set; }
    }
}