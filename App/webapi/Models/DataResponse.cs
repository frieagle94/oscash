namespace webapi.Models
{
    /// <summary>
    /// Represent a service/controller response with data
    /// </summary>
    public class DataResponse<T> : IResponse
    {
        /// <summary>
        /// Public ctor
        /// </summary>
        public DataResponse()
        {
        }
        /// <summary>
        /// Response data
        /// </summary>
        /// <value></value>
        public T data {get; set;}
        /// <summary>
        /// Message of response
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
        /// <summary>
        /// Result of response
        /// </summary>
        /// <value></value>
        public ResponseResult Result { get; set; }
    }
}