export class BaseModel {
    createdByID?: string;
    createdBy?: User;
    updatedByID?: string;
    updatedBy?: User;
    createdAt?: Date;
    updatedAt?: Date;
}

export class MenuProduct {
    menuID?: string;
    productID?: string;
    menu?: Menu;
    product?: Product;
}

export class MenuReceipt {
    menuID?: string;
    receiptID?: string;
    menu?: Menu;
    receipt?: Receipt;
    amount?: number;
}

export class Menu extends BaseModel {
    menuID?: string;
    name?: string;
    description?: string;
    price?: number;
    avalaible?: boolean;
    customizable?: boolean;
    productsAssociated?: MenuProduct[];
    receiptsAssociated?: MenuReceipt[];
}

export class Order extends BaseModel {
    orderID?: string;
    receiptID?: string;
    receipt?: Receipt;
    endedByID?: string;
    endedBy?: User;
    endedAt?: Date;
}

export class ProductReceipt {
    receiptID?: string;
    productID?: string;
    receipt?: Receipt;
    product?: Product;
    amount?: number;
}

export class Product extends BaseModel {
    productID?: string;
    name?: string;
    description?: string;
    productTypeID?: string;
    productType?: ProductType;
    price?: number;
    avalaible?: boolean;
    stockAvalaible?: number;
    canCustomize?: boolean;
    image?: string;
    menusAssociated?: MenuProduct[];
    receiptsAssociated?: ProductReceipt[];
}

export class ProductType extends BaseModel {
    productTypeID?: string;
    name?: string;
}

export class Receipt extends BaseModel {
    receiptID?: string;
    payed?: boolean;
    totalPrice?: number;
    discount?: number;
    notes?: string;
    productsAssociated?: ProductReceipt[];
    menusAssociated?: MenuReceipt[];
}

export class Session {
    sessionID?: string;
    userID?: string;
    user?: User;
    isValid?: boolean;
    startedAt?: Date;
    endedAt?: Date;
}

export class User {
    userID?: string;
    login?: string;
    userTypeID?: string;
    userType?: UserType;
    createdAt?: Date;
    name?: string;
}

export class UserType {
    userTypeID?: string;
    isAdmin?: boolean;
    name?: string;
}

export enum ResponseResult {
    Success = 0,
    Error = 1,
    Warning = 2
}

export class IResponse {
    message?: string;
    result?: ResponseResult;
}

export class DataResponse<T> extends IResponse {
    data?: T;
    message?: string;
    result?: ResponseResult;
}

export class Response extends IResponse {
    message?: string;
    result?: ResponseResult;
}

/**
 * Fake GUID
 */
export class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

export class LocalStorageKeys {
    static productsSelectedList = "productsSelectedList";
    static menusSelectedList = "menusSelectedList";
    static loginSessionKey = "loginSessionKey";
    static receiptNotes = "receiptNotes";
}