# OSCash

*Source repository per il progetto d'esame del corso di Laboratorio di Progettazione, AA 19/20*

> Software gestionale che ha come obiettivo quello di semplificare la vita di ogni povero cassiere di feste, sagre, oratori o qualsiasi altra manifestazione. Lo scopo di questo progetto è quello di fornire uno strumento il più flessibile possibile, leggero, snello e portatile, ma allo stesso tempo completo, per la gestione di una cassa non fiscale per feste più o meno grandi (scuole, sagre, feste paesane, oratori. 

**Idee e requisiti**
* Web app: unico server in grado di gestire più istanze di casse
* Retrocompatibile su dispositivi datati
* Server remoto/locale (su macchina dedicata/virtuale o stessa macchina) anche senza connessione internet
* OPEN SOURCE
* o	.NET Core
* o	PostgreSQL
* o	TS + JQuery + Bootstrap
* o	Libreria per binding?
* CROSS PLATFORM
* Server portabile anche su simil Raspberry PI
* Multidispositivo: interfaccia web per desktop, smartphone, tablet, palmari, ecc
* Multitenant: 1 istanza di app -> X tenant di ”feste” -> Y dispositivi di cassa (Pensato per un’organizzazione che fa tanti eventi a se stanti)

**Idee per il futuro**
* Applicazione server fornita a cliente per sua installazione, fino a tot dispositivi contemporanei
* Licensing per:
* o	Deploy dedicato in datacenter 
* o	Più di un tot di dispositivi contemporanei
* o	Funzionalità premium?

**Come muoversi?**
1. Minimum valuable product completamente open source monotenant
2. Multitenant
3. Eventuale licensing?