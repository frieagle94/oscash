# Appunti Progetto OSCash

Per il corso di: Laboratorio di Progettazione



> Software gestionale che ha come obiettivo quello di semplificare la vita di ogni povero cassiere di feste, sagre, oratori o qualsiasi altra manifestazione. Lo scopo di questo progetto è quello di fornire uno strumento il più flessibile possibile, leggero, snello e portatile, ma allo stesso tempo completo, per la gestione di una cassa non fiscale per feste più o meno grandi (scuole, sagre, feste paesane, oratori).



Il Software sviluppato fa parte del laboratorio di Progettazione Software per il corso di laurea magistrale Informatica in Bicocca.



**Funzionalità di base**

La funzionalità base del software è quella di dare la possibilità ad un cassiere di selezionare all'interno di un'interfaccia dei prodotti inseriti all'interno del software, comporre uno scontrino (non fiscale) tenendo traccia degli articoli ordinati.

L'interfaccia dovrà essere più funzionale possibile, fruibile anche con comandi touch e su tutti i dispositivi, quindi responsive anche per i cellulari.



**Feature base**

Il Progetto avrà due macro funzionalità base:

1. Legata ai prodotti e al cassiere: ovvero l'interfaccia base che consentirà di selezionare i prodotti e di generare uno scontrino, a tal proposito questa macro funzionalità di compone di altre due sotto funzionalità:
   - Interfaccia: Interfaccia di selezione dei prodotti disponibili con relativo prezzo
   - Scontrino: generazione dello scontrino con i prodotti selezionati

È importante sottolineare che i Prodotti potranno anche essere raggruppati all'interno di tipologie di prodotti (ad esempio: bibite, panini, ...)

Inoltre sarà possibile avere dei menù contenenti più prodotti (bibita + panino) contenente un prezzo diverso dai singoli prodotti.



2. Funzionalità legata alla gestione dei prodotti: sarà una funzionalità di amministrazione del sistema che consentirà di inserire dei prodotti all'interno del database con un'interfaccia amministratore personalizzandone le caratteristiche come: prezzo, tipologia, quantità, immagine di riferimento, nome ecc...

   Inoltre sarà possibile all'interno della funzionalità vedere la disponibilità dei prodotti in magazzino e verificare il bilancio delle entrate rispetto ai prodotti venduti.

   Questa seconda funzionalità di comporrà quindi delle seguenti microfunzionalità:

   - Inserimento e gestione prodotti
   - Bilancio
   - Magazzino



Inoltre sarebbe interessante avere una terza funzionalità base legata alla reportistica dell'evento che consentirà di tenere traccia dei prodotti venduti nel tempo, il costo medio, tipologie di prodotti più venduti, disponibilità rimanenti e soldi ricevuti.



**Requisiti Architetturali**

Per l'applicazione che intendiamo realizzare sono stati definiti alcuni requisiti importanti per un'applicazione di questo tipo:

1. Facilità di utilizzo e interazione su tutti i dispositivi (responsive)
2. Indipendenza da sistema operativo e da hardware e software
3. Sviluppata con tecnologie open source
4. Sviluppata con tecnologie web (si comporrà di un Front-end che sarà interagibile da diversi client e di un back-end posizionato su un dispositivo server durante l'evento)



A tal proposito l'architettura ad alto livello che intendiamo sviluppare sarà la seguente

![Architettura](./Risorse/OSCash-architetturaBase.png)

L'architettura più nel dettaglio potrà seguire due possibili applicazioni.

1. Distribuita
2. Centralizzata

Per la descrizione più dettagliata di queste due architetture si rimanda al documento ufficiale di progetto.



Le tecnologie usate per lo sviluppo saranno:

- Front-end
  - Vue.js
  - Bootstrap (CSS e HTML5)
  - Javascript
- Back-end
  - .Net Core
  - Entity Framework Core
  - Database: PostgreSQL (eventualmente MySQL)
- Architetturali
  - Gitlab
  - Docker
  - Azure Dev Ops
  - Microsoft Azure per la parte cloud



A tal proposito per l'implementazione della soluzione ci saranno due modalità di deploy e rilascio del software sviluppato:

- On Premise: utilizzando dei docker container con all'interno la soluzione
- On Cloud: utilizzando Microsoft Azure e una pipeline di Build automatica



**L'architettura della repository**

Per la gestione del codice verrà utilizzata come strumento di lavoro condiviso GitLab.

L'architettura e l'organizzazione delle branch sarà la seguente:

![Repository](/Users/jeydi/Dropbox/Progetti/UNIVERSITÀ/oscash/Appunti/Risorse/OSCash-GitLabStructure.png)

## Estensioni Visual Studio Code

- Code Runner
- Docker
- docs-markdown
- docs-preview
- Markdown All In One
- Markdown+Math
- markdownlint
- Gitlens
- LaTex Workshop
- LaTex Table Helper
- latex-formatter
- YAML
- Todo Tree
- Bookmarks
- vscode-icons
- vue
- Vue2 Snippets
- Vue VSCode snippets
- Vue VS Code Extension Pack
- PostgreSQL (Chris Kolkman)
- PostgreSQL (Microsoft)
- Project Manager



## Versioni Software

Node: 12.16.1

Net Core: 3.1

Vue.js: 2.6.11

Vue cli: 4.2.3

npm: 6.14.4




## Comandi Docker

#### Docker Compose

Lanciare compose per il database

```bash
docker-compose -f docker-compose-db.yml up -d
```



#### Net Core

Build an image (ad esempio webapi image) andando nella cartella della web api:

```bash
docker build -t oscash/webapi .
```

Dopo di che lanciare l'immagine facendo:

```cmd
docker run -p 8080:80 -d --name oscash-webapi oscash/webapi
```

Una volta fatto il run sarà necessario startare il container (in teoria mettendo -d all'interno del run non è più necessario perchè parte già in detach mode)

```bash
docker start oscash-webapi
```

Per stoppare il container fare

```bash
docker stop oscash-webapi
```

Attenzione: quando lanciate il container sarà tutto in http e non https, ad esempio:

```
http://localhost:8080/WeatherForecast
```

TODO: capire come mettere HTTPS anche su docker in questo caso!!!

#### PosgreSQL

Scaricare l'immagine ufficiale di PosgreSQL

```bash
docker pull postgres:12.2-alpine
```

Run the image (importante mettersi all'interno della cartella APP di progetto, se così non fosse ricordarsi di modificare il path all'interno del volume -v)

```bash
docker run -p 5432:5432 -d -e POSTGRES_PASSWORD=oscashDB42 -e POSTGRES_USER=admin-oscash -e POSTGRES_DB=oscash-db -v $HOME/DockerShared/postgres:/var/lib/postgresql/data --name oscash-db postgres:latest
```

*Connettersi al Db dopo che il docker sta runnando con tool locale*

Si può anche connettersi direttamente al container per eseguire query e informazioni direttamente lì

```bash
docker exec -it oscash-db bash
```

Utilizzare comandi postgres all'interno del docker

```bash
psql -h localhost -U admin-oscash
```



*Comandi utili su PostgreSQL*

Vedere l'elenco dei database `\l`

Connettersi ad un database `\c`

Visualizzare elenco utenti `\du`

Creare un database `create database test;` attenzione: ricordarsi di mettere sempre punto e virgola alla fine altrimenti non verrà committato il comando

 

Si possono usare altri tools tipo: PGAdmin (pgadmin 4): https://www.pgadmin.org/download/



*Backup and restore del db* (ancora da testare)

```bash
docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
```

Restore

```bash
cat your_dump.sql | docker exec -i your-db-container psql -U postgres
```





## Comandi Net Core

Creare un nuovo progetto web api con una specifica versione del framework

```bash
dotnet new webapi -n webapi -f netcoreapp3.1
```

Buildare il progetto

```bash
dotnet build
```

Lanciare il progetto

```bash
dotnet run
```
#### Entity Framework

Per creare lo schema database occorre creare una migrazione del modello.
Eseguire il comando:

```bash
dotnet ef migrations add InitialCreate
```

Per aggiornare lo schema, sulla base di una migrazione creata:

```bash
dotnet ef database update
```

Eliminare il DB:

```bash
dotnet ef database drop
```


**ATTENZIONE**

Probabilmente sarà necessario generare un certificato per HTTPS usando versione 3.1 del framework in quanto utilizza HTTPS come default.

Per generare un certificato di dev fare

```bash
dotnet dev-certs https --trust
```

seguire le istruzioni da terminale

se ci fossero problemi (su mac) usare questa guida: https://rimdev.io/solving-dotnet-core-https-development-certificate-issues-on-macos/



## Comandi Vue

Creare un nuovo progetto vue con un nome per il progetto (recarsi nella cartella dove creare il progetto)

```bash
vue create [nome_progetto]
```
Seguire le istruzioni per installare e personalizzare i plugin da usare nel progetto
--> typescript / route / babel / ...

Per usare Axios (componente per eseguire chiamate HTTP)
```bash
npm install axios
```


## Articoli e documentazione



https://medium.com/@saniales/0-penny-architecture-extended-adding-extra-free-services-to-a-well-known-model-26c2ba39b59a



#### NetCore

Guida ufficiale di Microsoft sulla creazione di una backend WebAPI app con .Net Core (fatta molto bene)

https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.1&tabs=visual-studio-code

Pagina di installazione Netcore e ASPNET Core : https://dotnet.microsoft.com/download/dotnet-core/3.1

Bellissimo blog riguardo .net core: https://dotnetplaybook.com

Video per creare backend project with docker and .net core: https://www.youtube.com/watch?v=f0lMGPB10bM

.Net Core official documentation Docker: https://docs.docker.com/engine/examples/dotnetcore/

.Net Core with SQL official documentation Docker: https://docs.docker.com/compose/aspnet-mssql-compose/

Documentazione ufficiale Net Core con Docker (creare dockerfile): https://docs.microsoft.com/en-us/dotnet/core/docker/build-container





#### Docker

Settare i Path assoluti e relativi nei volumes con Docker: [https://stackoverflow.com/questions/41485217/mount-current-directory-as-a-volume-in-docker-on-windows-10](https://www.google.com/url?q=https://stackoverflow.com/questions/41485217/mount-current-directory-as-a-volume-in-docker-on-windows-10&sa=D&source=hangouts&ust=1584958243456000&usg=AFQjCNGEnIs3Jl-mYmqIG-HZKKH_RzFFEg)



Tutorial Docker:

Capire gli errori di docker: https://medium.com/better-programming/understanding-docker-container-exit-codes-5ee79a1d58f6

#### VUE

Tutorial Vue.js: https://youtu.be/Wy9q22isx3U



#### POSTGRESQL

PostgreSQL official documentation Docker: https://docs.docker.com/engine/examples/postgresql_service/

Tutorial per usarlo con Docker: https://www.youtube.com/watch?v=G3gnMSyX-XM



